package com.javaStudy.goClimbApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoClimbApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoClimbApplication.class, args);
	}
}
