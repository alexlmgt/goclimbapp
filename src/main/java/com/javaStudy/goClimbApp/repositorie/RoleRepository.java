package com.javaStudy.goClimbApp.repositorie;

import com.javaStudy.goClimbApp.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
