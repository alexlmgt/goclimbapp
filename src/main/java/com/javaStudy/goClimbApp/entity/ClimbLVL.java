package com.javaStudy.goClimbApp.entity;

public enum ClimbLVL {
    NEWBIE,
    AVERAGE,
    PRO,
    TOP;
}
