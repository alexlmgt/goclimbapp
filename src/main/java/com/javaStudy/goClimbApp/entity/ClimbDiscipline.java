package com.javaStudy.goClimbApp.entity;

public enum ClimbDiscipline {
    BOULDERING,
    LEAD,
    SPEED;
}
