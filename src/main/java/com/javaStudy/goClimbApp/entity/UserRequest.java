package com.javaStudy.goClimbApp.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table (name="requests")
@Data
public class UserRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private ClimbDiscipline discipline;
    private ClimbLVL lvl;

}
